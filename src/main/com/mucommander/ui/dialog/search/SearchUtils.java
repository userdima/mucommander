package com.mucommander.ui.dialog.search;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Some usefull methods and functions used in
 * searching files.
 * 
 * @author zubr
 */
public class SearchUtils {
    public static int MATCH_GLOBAL = 0x200;
    
    public static String truncateText(String str, int maxLen, int lenFromEnd) {
        int len;
        if (str != null && (len = str.length()) > maxLen) {
            return str.substring(0, maxLen - lenFromEnd - 3) + "..."
                    + str.substring(len - lenFromEnd, len);
        }
        return str;
    }
    
    public static String wildcardToRegex(String wildcard) {
        StringBuilder str = new StringBuilder(wildcard.length());
        String[] parts = wildcard.split(";");
        for (String part: parts) {
            // open 'orred' group
            str.append("("); // was ^
            for (int i = 0; i < part.length(); i++) {
                char c = part.charAt(i);
                switch (c) {
                    case '*':
                        str.append(".*");
                        break;
                    case '?':
                        str.append(".");
                        break;
                    // escape special regexp-characters
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '$':
                    case '^':
                    case '.':
                    case '{':
                    case '}':
                    case '|':
                    case '\\':
                        str.append("\\");
                        str.append(c);
                        break;
                    default:
                        str.append(c);
                        break;
                }
            }
            // close group
            str.append(")|"); // was $
        }
        if (str.length() > 0) {
            str.deleteCharAt(str.length() - 1);
        }
        return str.toString();
    }
    
    /**
     * Check regexp for validity.
     * @param regexp regexp string to check
     * @return error description, if any
     */
    public static String getRegexpError(String regexp) {
        try {
            Pattern.compile(regexp);
            return null;
        }
        catch (PatternSyntaxException ex) {
            return ex.getLocalizedMessage();
        }
    }

    public static Matcher getPatternMatcher(String pattern, boolean useRegEx) {
        if (!useRegEx)
            pattern = wildcardToRegex(pattern);
        // handle javascript-style pattern
        int flags = 0;
        if (pattern.length() > 0 && pattern.charAt(0) == '/') {
            String[] parts = pattern.split("/");
            if (parts.length == 3) {
                if (parts[2].indexOf('i') != -1)
                    flags |= Pattern.CASE_INSENSITIVE;
                if (parts[2].indexOf('m') != -1)
                    flags |= Pattern.MULTILINE;
                if (parts[2].indexOf('g') != -1)
                    flags |= MATCH_GLOBAL;
                pattern = parts[1];
            }
            // strip that parts
            if (parts.length > 1)
                pattern = parts[1];
        }
        return Pattern.compile(pattern, flags).matcher("");
    }
}
