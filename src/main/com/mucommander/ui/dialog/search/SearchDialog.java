/*
 * This file is part of muCommander, http://www.mucommander.com
 * Copyright (C) 2002-2012 Maxence Bernard
 *
 * muCommander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * muCommander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mucommander.ui.dialog.search;

import com.mucommander.commons.file.AbstractFile;
import com.mucommander.commons.file.FileFactory;
import com.mucommander.core.LocationChanger;
import com.mucommander.ui.dialog.search.fs.*;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mucommander.history.HistoryManager;
import com.mucommander.text.Translator;
import com.mucommander.ui.dialog.DialogToolkit;
import com.mucommander.ui.dialog.InformationDialog;
import com.mucommander.ui.icon.SpinningDial;
import com.mucommander.ui.layout.AsyncPanel;
import com.mucommander.ui.layout.XBoxPanel;
import com.mucommander.ui.layout.YBoxPanel;
import com.mucommander.ui.main.MainFrame;
import com.mucommander.ui.viewer.FileFrame;
import com.mucommander.ui.viewer.ViewerRegistrar;
import com.mucommander.ui.viewer.text.TextViewer;
import java.awt.Color;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

// TODO: some theming

/**
 * Dialog used to execute some search queries.
 * 
 * @author Zubr
 */
public class SearchDialog extends JDialog implements ActionListener, KeyListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchDialog.class);
	
    // - UI components -------------------------------------------------------------------
    // -----------------------------------------------------------------------------------
    /** Main frame this dialog depends on. */
    private MainFrame mainFrame;
    /** Editable combo box used for search query. */
    private SearchComboBox filePatternCombo;
    private SearchComboBox fileContentCombo;
    /** Start/stop button. */
    private JButton       startStopButton;
    /** Cancel button. */
    private JButton       cancelButton;
    /** Clear shell history button. */
    private JButton       clearButton;
    private JButton       moveToPanelButton;
    private JButton       jumpToFileButton;
    private JButton       viewButton;
    private JButton       patternHelpButton;
    private Action        viewAction;
    /** Search subdirectories checkbox. */
    private JCheckBox     searchSubdirsCheck;
    /** Search text in files checkbox. */
    private JCheckBox     searchContentCheck;
    private JCheckBox     useRegExpCheck;
    private JLabel        searchStatusLabel;
    /** File list to display search results. */
    private JList         resultsList;
    private DefaultListModel<String> resultsListModel;
    /** Used to let the user known that the command is still running. */
    private SpinningDial  progressDial;
    private SearchRoot    currentSearchRoot;
    private Thread        currentSearchThread;

    // - Misc. class variables -----------------------------------------------------------
    // -----------------------------------------------------------------------------------
    /** Minimum dimensions for the dialog. */
    private final static Dimension MINIMUM_DIALOG_DIMENSION = new Dimension(800, 600);
    
    private final static String FILE_NAME_PATTERN_HISTORY_CATEGORY = "search.fileNamePattern";
    private final static String FILE_CONTENT_PATTERN_HISTORY_CATEGORY = "search.fileContentPattern";

    // - Initialisation ------------------------------------------------------------------
    // -----------------------------------------------------------------------------------
    /**
     * Creates the dialog's search output area.
     * @return a scroll pane containing the dialog's search output area.
     */
    private JScrollPane createOutputArea() {
        resultsListModel = new DefaultListModel();
        resultsList = new JList(resultsListModel);
        // setup 'view' action
        viewAction = new AbstractAction("view") {
            @Override
            public void actionPerformed(ActionEvent e) {
                String fileUrl = (String)resultsList.getSelectedValue();
                if (fileUrl == null)
                    return;

                AbstractFile file = FileFactory.getFile(fileUrl);
                if (file != null) {
                    FileFrame frame = ViewerRegistrar.createViewerFrame(mainFrame, file, null);
                    // some awfull peace of code, NEVER do such things
                    try {
                        JPanel panel = (JPanel)frame.getContentPane();
                        AsyncPanel asyncPanel = (AsyncPanel)panel.getComponent(0);
                        TextViewer viewer = (TextViewer)asyncPanel.getTargetComponent();
                        viewer.setSearchString(fileContentCombo.getQuery().toLowerCase());
                    }
                    catch (Exception ex) {
                        // I DON'T CARE
                    }
                }
            }
        };
        viewAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("F3"));
        resultsList.getActionMap().put("viewAction", viewAction);
        resultsList.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
            (KeyStroke) viewAction.getValue(Action.ACCELERATOR_KEY), "viewAction");
        resultsList.addKeyListener(this);

        return new JScrollPane(resultsList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    /**
     * Creates the search input part of the dialog.
     * @return the search input part of the dialog.
     */
    private YBoxPanel createInputArea() {
        YBoxPanel mainPanel = new YBoxPanel();

        XBoxPanel filePatternPanel = new XBoxPanel();
        filePatternCombo = new SearchComboBox(this, FILE_NAME_PATTERN_HISTORY_CATEGORY);
        
        JLabel filePatternLabel = new JLabel(Translator.get("search_dialog.find_file_folder_description")+":");
        filePatternLabel.setLabelFor(filePatternCombo.getTextField());
        filePatternLabel.setDisplayedMnemonic('f');
        filePatternPanel.add(filePatternLabel);
        filePatternPanel.add(Box.createHorizontalGlue());
        patternHelpButton = new JButton(Translator.get("search_dialog.pattern_help"));
        patternHelpButton.addActionListener(this);
        filePatternPanel.add(patternHelpButton);
        mainPanel.add(filePatternPanel);
        
        filePatternCombo.getTextField().addKeyListener(this);
        filePatternCombo.setEnabled(true);
        mainPanel.add(filePatternCombo);
        
        fileContentCombo = new SearchComboBox(this, FILE_CONTENT_PATTERN_HISTORY_CATEGORY);
        JLabel fileContentLabel = new JLabel(Translator.get("search_dialog.find_file_content_description")+":");
        filePatternLabel.setLabelFor(fileContentCombo);
        mainPanel.add(fileContentLabel);
        fileContentCombo.getTextField().addKeyListener(this);
        fileContentCombo.setEnabled(false);
        mainPanel.add(fileContentCombo);
        
        // Options section
        mainPanel.add(new JLabel(Translator.get("search_dialog.settings")+":"));
        
        searchSubdirsCheck = new JCheckBox(Translator.get("search_dialog.search_subdirs"));
        searchSubdirsCheck.setMnemonic(KeyEvent.VK_D);
        searchSubdirsCheck.addActionListener(this);
        searchSubdirsCheck.setSelected(true);
        mainPanel.add(searchSubdirsCheck);
        
        searchContentCheck = new JCheckBox(Translator.get("search_dialog.search_content"));
        searchContentCheck.setMnemonic(KeyEvent.VK_T);
        searchContentCheck.addActionListener(this);
        searchContentCheck.setSelected(false);
        mainPanel.add(searchContentCheck);
        
        useRegExpCheck = new JCheckBox(Translator.get("search_dialog.use_regexp"));
        useRegExpCheck.setMnemonic(KeyEvent.VK_E);
        useRegExpCheck.addActionListener(this);
        useRegExpCheck.setEnabled(false);
        mainPanel.add(useRegExpCheck);

        mainPanel.addSpace(10);

        XBoxPanel labelPanel = new XBoxPanel();
        labelPanel.add(new JLabel(Translator.get("search_dialog.search_results")+":"));
        labelPanel.addSpace(10);
        progressDial = new SpinningDial();
        labelPanel.add(new JLabel(progressDial));
        labelPanel.addSpace(10);
        searchStatusLabel = new JLabel();
        labelPanel.add(searchStatusLabel);
        mainPanel.add(labelPanel);

        return mainPanel;
    }

    /**
     * Creates a panel containing the dialog's buttons.
     * @return a panel containing the dialog's buttons.
     */
    private XBoxPanel createButtonsArea() {
        XBoxPanel buttonsPanel = new XBoxPanel();

        clearButton = new JButton(Translator.get("search_dialog.clear_history"));
        clearButton.addActionListener(this);
        clearButton.setMnemonic('h');
        buttonsPanel.add(clearButton);
        
        moveToPanelButton = new JButton(Translator.get("search_dialog.move_to_panel"));
        moveToPanelButton.addActionListener(this);
        moveToPanelButton.setMnemonic('p');
        buttonsPanel.add(moveToPanelButton);
        
        viewButton = new JButton(Translator.get("search_dialog.view"));
        //viewButton.getActionMap().put("viewAction", viewAction);
        viewButton.addActionListener(this);
        viewButton.setMnemonic('v');
        buttonsPanel.add(viewButton);
        
        jumpToFileButton = new JButton(Translator.get("search_dialog.jump_to_file"));
        jumpToFileButton.addActionListener(this);
        jumpToFileButton.setMnemonic('j');
        buttonsPanel.add(jumpToFileButton);

        buttonsPanel.add(Box.createHorizontalGlue());

        buttonsPanel.add(DialogToolkit.createOKCancelPanel(
                startStopButton = new JButton(Translator.get("search_dialog.start")),
                cancelButton = new JButton(Translator.get("cancel")),
                getRootPane(),
                this));

        return buttonsPanel;
    }

    /**
     * Creates and displays a new SearchDialog.
     * @param mainFrame the main frame this dialog is attached to.
     */
    public SearchDialog(MainFrame mainFrame) {
        super(mainFrame);
        this.mainFrame = mainFrame;
        this.currentSearchRoot = null;
        this.currentSearchThread = null;
		
        // Initializes the dialog's UI.
        JPanel contentPane = (JPanel)getContentPane();
        contentPane.add(createInputArea(), BorderLayout.NORTH);
        contentPane.add(createOutputArea(), BorderLayout.CENTER);
        contentPane.add(createButtonsArea(), BorderLayout.SOUTH);
        contentPane.setBorder(new EmptyBorder(6, 8, 6, 8));

        // Sets default items.
        addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                filePatternCombo.getTextField().requestFocusInWindow();
            }
        });
        getRootPane().setDefaultButton(startStopButton);

        // Makes sure that any running process will be killed when the dialog is closed.
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                stopSearch();
            }
        });

        // Sets the dialog's minimum size.
        setMinimumSize(MINIMUM_DIALOG_DIMENSION);
        // center dialog
        setLocation(mainFrame.getX()+(mainFrame.getWidth()-getWidth())/2,
                mainFrame.getY()+(mainFrame.getHeight()-getHeight())/2);
    }

    // - ActionListener code -------------------------------------------------------------
    // -----------------------------------------------------------------------------------
    /**
     * Notifies the SearchDialog that an action has been performed.
     * @param e describes the action that occured.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if(source == clearButton) {
            HistoryManager.clear(FILE_NAME_PATTERN_HISTORY_CATEGORY);
            HistoryManager.clear(FILE_CONTENT_PATTERN_HISTORY_CATEGORY);
        }
        else if (source == moveToPanelButton) {
            final SearchRoot root = SearchProtocolProvider.getSearchRoot();
            root.clearFiles();
            // don't forget the '..'
            root.setParent(mainFrame.getActivePanel().getCurrentFolder());
            // fill with search results
            for (int i = 0; i < resultsListModel.getSize(); i++)
                root.addFile(FileFactory.getFile(resultsListModel.get(i)));
            mainFrame.getActivePanel().tryChangeCurrentFolder(root);
            // nothing to do here
            dispose();
        }
        else if (source == viewButton) {
            viewAction.actionPerformed(e);
        }
        else if (source == jumpToFileButton) {
            // bad hack...
            keyPressed(new KeyEvent(resultsList, 0, 0, 0, KeyEvent.VK_ENTER, '\n'));
        }
        else if (source == startStopButton) {
            if (!isSearchRunning())
                startSearch();
            else
                stopSearch();
        }
        else if (source == cancelButton) {
            stopSearch();
            dispose();
        }
        else if (source == patternHelpButton) {
            InformationDialog.showDialog(InformationDialog.INFORMATION_DIALOG_TYPE,
                    this, Translator.get("search_dialog.pattern_help"),
                    Translator.get("search_dialog.pattern_help.caption"),
                    Translator.get("search_dialog.pattern_help.content"), null);
        }
        else if (source == searchContentCheck) {
            useRegExpCheck.setEnabled(searchContentCheck.isSelected());
            fileContentCombo.setEnabled(searchContentCheck.isSelected());
            fileContentCombo.requestFocus();
        }
        else if (source == useRegExpCheck) {
            //validateComboBoxPattern(filePatternCombo);
            validateComboBoxPattern(fileContentCombo);
        }
    }
    
    private void addHistoryItem(String category, String item) {
        Iterator<String> it = HistoryManager.getHistoryIterator(category);
        while (it.hasNext()) {
            if (it.next().equals(item)) {
                it.remove();
                break;
            }
        }
        HistoryManager.add(category, item);
    }


    public boolean isSearchRunning() {
        return (currentSearchThread != null);
    }
    public void startSearch() {
        if (isSearchRunning())
            return;
        
        addHistoryItem(FILE_NAME_PATTERN_HISTORY_CATEGORY, filePatternCombo.getQuery());
        if (searchContentCheck.isSelected())
            addHistoryItem(FILE_CONTENT_PATTERN_HISTORY_CATEGORY, fileContentCombo.getQuery());
        
        try {
            resultsListModel.clear();
            currentSearchRoot = new SearchRoot();
            currentSearchThread = new SearchThread(
                    filePatternCombo.getQuery(),
                    fileContentCombo.getQuery(),
                    searchContentCheck.isSelected(),
                    searchSubdirsCheck.isSelected(),
                    useRegExpCheck.isSelected()
            );
            currentSearchThread.start();
            // update gui
            progressDial.setAnimated(true);
            startStopButton.setText(Translator.get("search_dialog.stop"));
        } catch (IOException ex) {
            // should never happen
        }
    }
    public void stopSearch() {
        if (!isSearchRunning())
            return;
        currentSearchThread.interrupt();
        currentSearchThread = null;
        // update gui
        progressDial.setAnimated(false);
        startStopButton.setText(Translator.get("search_dialog.start"));
        updateSearchStatus("");
    }

    /**
     * Add found file/folder to the output list.
     * Should not be called from gui thread.
     */
    private void addFoundFile(final AbstractFile file) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                resultsListModel.addElement(file.getAbsolutePath());
            }
        });
    }
    private void addFoundString(final String string) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                resultsListModel.addElement(string);
            }
        });
    }
    
    private void updateSearchStatus(String status) {
        searchStatusLabel.setText(status);
    }

    
    // - KeyListener code ----------------------------------------------------------------
    // -----------------------------------------------------------------------------------
    // These are used to validate regular expressions as they are typed
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getComponent() == resultsList && e.getKeyCode() == KeyEvent.VK_ENTER) {
            String filePath = (String)resultsList.getSelectedValue();
            if (filePath == null)
                return;
            // show the file
            AbstractFile file = FileFactory.getFile(filePath);
            LocationChanger.ChangeFolderThread thread = mainFrame.getActivePanel().
                    tryChangeCurrentFolder(file.getParent());
            thread.selectThisFileAfter(file);
            // get rid of self
            dispose();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getComponent().getParent() == fileContentCombo) {
            validateComboBoxPattern(fileContentCombo);
        }
    }
    
    private void validateComboBoxPattern(SearchComboBox cb) {
        String regexp = cb.getQuery();
        // convert if needed
        if (!useRegExpCheck.isSelected())
            regexp = SearchUtils.wildcardToRegex(regexp);
        // validate input and describe error
        String errorDesc = SearchUtils.getRegexpError(regexp);
        boolean hasError = (errorDesc != null);
        cb.setBackground(hasError ? Color.red : Color.white);
        cb.setToolTipText(hasError ? errorDesc : "");
    }
    
    /**
     * Search thread class.
     * 
     * Used actually to traverse directory tree and report found files.
     */
    class SearchThread extends Thread {
        private Matcher fileNameMatcher;
        private Matcher contentMatcher;
        private boolean searchSubdirs;
        
        SearchThread(String fileNamePattern, String contentPattern,
                boolean searchContent, boolean searchSubdirs, boolean useRegEx) {
            this.searchSubdirs = searchSubdirs;
            this.fileNameMatcher = Pattern.compile(SearchUtils.wildcardToRegex(fileNamePattern)).matcher("");
            this.contentMatcher = null;
            if (searchContent)
                this.contentMatcher = SearchUtils.getPatternMatcher(contentPattern, useRegEx);
        }
        
        private void iterateThroughFolder(AbstractFile folder) {
            try {
                AbstractFile[] children = folder.ls();
                for (AbstractFile child: children) {
                    // exit properly
                    if (isInterrupted())
                        return;
                    
                    // update some gui
                    updateSearchStatus("(" + child.getAbsolutePath() + ")");
                    
                    // check file name to match
                    fileNameMatcher.reset(child.getName());
                    if (fileNameMatcher.matches()) {
                        if (contentMatcher == null)
                            addFoundFile(child);
                        else {
                            // do some additional work to search in file
                            Scanner scanner = new Scanner(child.getInputStream());
                            if (scanner.findWithinHorizon(contentMatcher.pattern(), 0) != null)
                                addFoundFile(child);                            
                            scanner.close();
                        }
                    }
                }
                for (AbstractFile child: children) {
                    // exit properly
                    if (isInterrupted())
                        return;
                    
                    // update some gui
                    updateSearchStatus("(" + child.getAbsolutePath() + ")");
                    
                    // iterate further
                    if (searchSubdirs && child.isBrowsable() && !child.isSymlink())
                        iterateThroughFolder(child);
                }
            } catch (IOException ex) {
                // ignore everything
            }
        }

        @Override
        public void run() {
            AbstractFile root = mainFrame.getActiveTable().getFolderPanel().getCurrentFolder();
            iterateThroughFolder(root);
            
            // now we're done, clear ourselves
            stopSearch();
        }
        
    }
}
