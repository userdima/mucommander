/*
 * This file is part of muCommander, http://www.mucommander.com
 * Copyright (C) 2002-2012 Maxence Bernard
 *
 * muCommander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * muCommander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.mucommander.ui.dialog.search.fs;

import com.mucommander.bookmark.file.*;
import com.mucommander.bookmark.Bookmark;
import com.mucommander.bookmark.BookmarkManager;
import com.mucommander.commons.file.AbstractFile;
import com.mucommander.commons.file.FileFactory;
import com.mucommander.commons.file.FileURL;
import com.mucommander.commons.file.ProtocolProvider;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is the provider for the bookmark filesystem implemented by {@link com.mucommander.bookmark.file.BookmarkFile}.
 *
 * @author Nicolas Rinaudo
 * @see com.mucommander.bookmark.file.BookmarkFile
 */
public class SearchProtocolProvider implements ProtocolProvider {

    /** Protocol for the virtual bookmarks file system. */
    public static final String SEARCH = "search";
    
    private static SearchRoot root;
    
    static {
        try {
            root = new SearchRoot();
        } catch (IOException ex) {
            // should never happen
        }
    }

    public AbstractFile getFile(FileURL url, Object... instantiationParams) throws IOException {
        // If the URL contains a path but no host, it's illegal.
        // If it contains neither host nor path, we're browsing bookmarks://
        if(url.getHost() == null) {
            if(url.getPath().equals("/"))
                return new SearchRoot(url);
            throw new IOException();
        }

        // If the URL contains a host, look it up in the bookmark list and use that
        // as the root of the returned path.
        // TODO: create search from url
        throw new IOException();
    }
    
    public static SearchRoot getSearchRoot() {
        return root;
    }
}
