/**
 * Here comes some license...
 */

package com.mucommander.ui.action.impl;

import com.mucommander.commons.file.AbstractFile;
import com.mucommander.commons.file.FileFactory;
import com.mucommander.text.Translator;
import com.mucommander.ui.action.AbstractActionDescriptor;
import com.mucommander.ui.action.ActionCategories;
import com.mucommander.ui.action.ActionCategory;
import com.mucommander.ui.action.ActionDescriptor;
import com.mucommander.ui.action.ActionFactory;
import com.mucommander.ui.action.MuAction;
import com.mucommander.ui.dialog.InformationDialog;
import com.mucommander.ui.dialog.search.fs.SearchRoot;
import com.mucommander.ui.main.MainFrame;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import javax.swing.KeyStroke;

/**
 * This action pops up the 'Find file/folder' dialog that is used to walk the folder
 * hierarchy searching for specified files or folders.
 *
 * @author Zubr Kabbi
 */
public class LoadFileListAction extends MuAction {

    public LoadFileListAction(MainFrame mainFrame, Map<String,Object> properties) {
        super(mainFrame, properties);
    }

    @Override
    public void performAction() {
        AbstractFile inputFile = mainFrame.getActiveTable().getSelectedFile();
        if (inputFile == null) {
            InformationDialog.showWarningDialog(mainFrame, Translator.get("LoadFileList.select_file"));
            return;
        }
        try {
            final SearchRoot root = new SearchRoot();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputFile.getInputStream()));
            String curLine = reader.readLine();
            while (curLine != null) {
                AbstractFile file = FileFactory.getFile(curLine);
                if (file != null)
                    root.addFile(file);
                curLine = reader.readLine();
            }
            root.setParent(mainFrame.getInactivePanel().getCurrentFolder());
            // show that panel
            mainFrame.getInactivePanel().tryChangeCurrentFolder(root);
        }
        catch (IOException ex) {
            // ignore
        }
    }

	@Override
	public ActionDescriptor getDescriptor() {
		return new Descriptor();
	}

    public static class Factory implements ActionFactory {

		public MuAction createAction(MainFrame mainFrame, Map<String,Object> properties) {
			return new LoadFileListAction(mainFrame, properties);
		}
    }
    
    public static class Descriptor extends AbstractActionDescriptor {
    	public static final String ACTION_ID = "LoadFileList";
    	
		public String getId() { return ACTION_ID; }

		public ActionCategory getCategory() { return ActionCategories.WINDOW; }

		public KeyStroke getDefaultAltKeyStroke() { return null; }

		public KeyStroke getDefaultKeyStroke() { return null; }
    }
}
