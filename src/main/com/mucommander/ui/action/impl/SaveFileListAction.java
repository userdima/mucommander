/**
 * Here comes some license...
 */

package com.mucommander.ui.action.impl;

import com.mucommander.commons.file.AbstractFile;
import com.mucommander.text.Translator;
import com.mucommander.ui.action.AbstractActionDescriptor;
import com.mucommander.ui.action.ActionCategories;
import com.mucommander.ui.action.ActionCategory;
import com.mucommander.ui.action.ActionDescriptor;
import com.mucommander.ui.action.ActionFactory;
import com.mucommander.ui.action.InvokesDialog;
import com.mucommander.ui.action.MuAction;
import com.mucommander.ui.dialog.InformationDialog;
import com.mucommander.ui.main.MainFrame;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

/**
 * This action pops up the 'Find file/folder' dialog that is used to walk the folder
 * hierarchy searching for specified files or folders.
 *
 * @author Zubr Kabbi
 */
@InvokesDialog
public class SaveFileListAction extends MuAction {

    public SaveFileListAction(MainFrame mainFrame, Map<String,Object> properties) {
        super(mainFrame, properties);
    }

    @Override
    public void performAction() {
        AbstractFile folder = mainFrame.getActivePanel().getCurrentFolder();
        List<String> list = new ArrayList<String>();
        if (folder != null) {
            try {
                for (AbstractFile file: folder.ls())
                    list.add(file.getAbsolutePath());
            } catch (IOException ex) {
                // ignore
            }
        }
        
        System.err.println(mainFrame.getActivePanel().getCurrentFolder().getCanonicalPath());
        System.err.println(mainFrame.getActivePanel().getCurrentFolder().getAbsolutePath());
        JFileChooser fileChooser = new JFileChooser(
                mainFrame.getActivePanel().getCurrentFolder().getAbsolutePath());
        int result = fileChooser.showSaveDialog(mainFrame);
        if (result == JFileChooser.APPROVE_OPTION) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(new FileWriter(fileChooser.getSelectedFile()));
                for (String file: list) {
                    writer.println(file);
                }
            } catch (IOException ex) {
                InformationDialog.showErrorDialog(mainFrame, Translator.get("SaveFileList.save_error"));
            } finally {
                writer.close();
            }
        }
    }

	@Override
	public ActionDescriptor getDescriptor() {
		return new Descriptor();
	}

    public static class Factory implements ActionFactory {

		public MuAction createAction(MainFrame mainFrame, Map<String,Object> properties) {
			return new SaveFileListAction(mainFrame, properties);
		}
    }
    
    public static class Descriptor extends AbstractActionDescriptor {
    	public static final String ACTION_ID = "SaveFileList";
    	
		public String getId() { return ACTION_ID; }

		public ActionCategory getCategory() { return ActionCategories.WINDOW; }

		public KeyStroke getDefaultAltKeyStroke() { return null; }

		public KeyStroke getDefaultKeyStroke() { return null; }
    }
}
